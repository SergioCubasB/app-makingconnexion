import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import Categories from '../../data/Categories.json';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  
  categories!: Observable<any>;

  constructor() { }

  getCategories(){
    this.categories = new Observable(
      (
        subscribe => {
          subscribe.next(Categories);
        }
      )
    )

    return this.categories;
  }

}
