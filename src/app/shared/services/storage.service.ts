import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private products = new Subject();
  private acumTotal = new Subject();
  private amount = new Subject();

  private bag = new Subject();

  constructor() { 
  }

  getProducts(){
    const storage:any = localStorage.getItem("bag");
    const storageParse = JSON.parse(storage);

    if(storageParse){
      if(storageParse.hasOwnProperty('length')){
        return storageParse;
      }else{
        const array = [];
  
        array.push(storageParse);
        return array;
      }
    }else{
      return null;
    }
    
  }


  getProdctsToBag(){
    return this.products.asObservable();
  }

  changeProductsBag(product: any){
    this.products.next(product);
  }

  deleteProductToStorage(id: any){
    const storage:any = localStorage.getItem("bag");
    const storageParse = JSON.parse(storage);
    var product:any;

    if(storage){

      if(storageParse.hasOwnProperty(length)){

        product = storageParse.filter(
          (element:any, index:any) => {
            return element.id != parseInt(id);
          }
        )
  
      }else{
        const array = [];
        array.push(storageParse);
  
        product = array.filter(
          (element:any, index:any) => {
            return element.id != parseInt(id);
          }
        )
      }
  
      localStorage.removeItem('bag');
  
      const productsInBagStringify = JSON.stringify(product);
      localStorage.setItem('bag', productsInBagStringify);
  
      this.getPriceTotal();
  
      this.getAmountProduct();
  
      //this.products.next(storageParse);
      this.changeProductsBag(storageParse);
    }
    
  }

  /* Bag */
  getAmountProduct(){
    const storage:any = localStorage.getItem("bag");
    const storageParse = JSON.parse(storage);

    this.changeCountBag(storageParse ? storageParse.length : 0);

    return storageParse ? storageParse.length : 0;
  }

  getTotalProductsInBag(){
    return this.bag.asObservable();
  }

  changeCountBag(count: any){
    this.bag.next(count);
  }


  /* Precio */
  getPrice(){
    this.getTotalProductsInBag();
    return this.acumTotal.asObservable();
  }
  
  getPriceTotal(){
    const storage:any = localStorage.getItem("bag");
    const storageParse = JSON.parse(storage);
    var acumTotal = 0;
    
    if(storage){
      
      if(storageParse.hasOwnProperty(length)){
        storageParse.forEach( (element:any) => {
          acumTotal += element.bag[0].total;
        }); 
    
        this.acumTotal.next(acumTotal);
    
        return this.acumTotal;
      }else{

        if(storageParse.length === 0){
          localStorage.removeItem("bag");
          
          this.acumTotal.next(0);
          return this.acumTotal;
        }else{
          localStorage.removeItem("bag");

          const amount:any = storageParse.bag[0].total;
          this.acumTotal.next(amount);
          
          return this.acumTotal;
        }

      }
    }else{
      return null;      
    }

  }

}
