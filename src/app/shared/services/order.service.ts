import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private env =  environment;

  constructor(
    private http: HttpClient
  ) { 
  }

  postOrder(order: any){
    return this.http.post(this.env.api + `/order`,order);
  }

}
