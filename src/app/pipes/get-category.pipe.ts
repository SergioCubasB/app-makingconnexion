import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../pages/shop/products/interfaces/product';

@Pipe({
  name: 'getCategory'
})
export class GetCategoryPipe implements PipeTransform {

  transform(product: any, indice: Number): unknown {
    return product.category[0].name;
  }

}
